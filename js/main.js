$(document).ready(function() {

  $(window).load(function() {
    $("#content-loader").delay(1000).fadeOut(1000);
  });

  $("#navigation .prev").click(function(e) {
    window.slider.goToPrevSlide();
  });

  $("#navigation .next").click(function(e) {
    window.slider.goToNextSlide();
  });

  $("#navigation .to-contents").click(function(e) {
    window.slider.goToSlide(1);
  });

  $(document).keydown(function(e) {
    var previous = 37, next = 39;
    switch (e.keyCode) {
      case previous:
        window.slider.goToPrevSlide();
        break;
      case next:
        window.slider.goToNextSlide();
        break;
    }
  });
  
  $("#navigation .to-begin").click(function(e) {
    var sectionNumber = $(this).text();
    switch (sectionNumber) {
      case "1":
        window.slider.goToSlide(2);
        break;
      case "2":
        window.slider.goToSlide(12);
        break;
      case "3":
        window.slider.goToSlide(25);
        break;
      case "4":
        window.slider.goToSlide(33);
        break;
      case "5":
        window.slider.goToSlide(40);
        break;
      case "6":
        window.slider.goToSlide(47);
        break;
      default:
        window.slider.goToSlide(0);
    }
  });

  var sliderElement = ".bxslider";
  var slideElement = ".slide";
  var slideMargin = 5;
  $.isTouch = 'ontouchstart' in window;
  var loadedPages = [0];

  function setSize(slider, slideMargin) {
    var scaleX = 1;
    var scaleY = 0.90;
    var scaleContentHeight = 0.8;
    var newWidth = Math.round($(window).width() * scaleX);
    var newHeight = Math.round($(window).height() * scaleY);
    var newSlideContentHeight = Math.round(newHeight * scaleContentHeight);
    var newMarginTop = Math.round(($(window).height() - newHeight) / 2 - 1);
    var newTranslate3d = newWidth * (-1) * window.slider.getCurrentSlide() - (window.slider.getCurrentSlide() * slideMargin);
    $(slider).children().css('width', newWidth + "px");
    $('.bx-wrapper').css('max-width', newWidth + "px");
    $('.bx-wrapper').css('margin', newMarginTop + "px auto");
    $(slider).children().css('height', newHeight + "px");
    $('.bx-viewport').css('height', newHeight + "px");
    $(slider).css("transform", "translate3d(" + newTranslate3d + "px, 0px, 0px)");
  }

  function drawSlide(slideNum) {
    var childNum = slideNum + 1;
    if ($.inArray(slideNum, loadedPages) == -1 && slideNum > 0 && slideNum < window.slider.getSlideCount()) {
      $(sliderElement + " " + slideElement + ":nth-child(" + childNum + ") .slide-content").fadeOut(500, function() {
        $(sliderElement + " " + slideElement + ":nth-child(" + childNum + ")").addClass("bx-loading");
        $(sliderElement + " " + slideElement + ":nth-child(" + childNum + ") .slide-content").load( "content/" + slideNum + ".html", function(response, status, xhr) {
          switch (status) {
            case "success":
              loadedPages.push(slideNum);
              $(sliderElement + " " + slideElement + ":nth-child(" + childNum + ") .slide-content").fadeIn(1000);
              $(sliderElement + " " + slideElement + ":nth-child(" + childNum + ")").removeClass("bx-loading");
              break;
            case "error":
              alert("Невозможно загрузить содержимое.");
              break;
          }
        });
      });
    }
  }

  window.slider = $(sliderElement).bxSlider({
    minSlides: 1,
    maxSlides: 1,
    infiniteLoop: false,
    hideControlOnEnd: true,
    controls: false,
    pager: false,
    slideWidth: 500,
    responsive: false,
    swipeThreshold: Math.abs(Math.round($(window).width() / 2 - 50)),
    slideMargin: slideMargin,
    speed: 1000,
    onSlideAfter: function(el, fromSlide, toSlide) {
      Hash.go('page/' + toSlide).update();
      drawSlide(toSlide);
      drawSlide(toSlide + 1);
      drawSlide(toSlide + 2);
      var newPageNum = "-";
      switch (true) {
        case (toSlide > 1 && toSlide < 12):
          newPageNum = "1";
          break;
        case (toSlide > 11 && toSlide < 25):
          newPageNum = "2";
          break;
        case (toSlide > 24 && toSlide < 33):
          newPageNum = "3";
          break;
        case (toSlide > 32 && toSlide < 40):
          newPageNum = "4";
          break;
        case (toSlide > 39 && toSlide < 47):
          newPageNum = "5";
          break;
        case (toSlide > 46):
          newPageNum = "6";
          break;
      }
      $("#navigation .to-begin").text(newPageNum);
      // if ($.inArray(toSlide, loadedPages) == -1) {
      //   $(sliderElement + " " + slideElement + ":nth-child(" + (toSlide + 1) + ") .slide-content").fadeOut(500, function() {
      //     $(sliderElement + " " + slideElement + ":nth-child(" + (toSlide + 1) + ")").addClass("bx-loading");
      //     $(sliderElement + " " + slideElement + ":nth-child(" + (toSlide + 1) + ") .slide-content").load( "content/" + toSlide + ".html", function(response, status, xhr) {
      //       switch (status) {
      //         case "success":
      //           loadedPages.push(toSlide);
      //           $(sliderElement + " " + slideElement + ":nth-child(" + (toSlide + 1) + ") .slide-content").fadeIn(1000);
      //           $(sliderElement + " " + slideElement + ":nth-child(" + (toSlide + 1) + ")").removeClass("bx-loading");
      //           break;
      //         case "error":
      //           alert("Невозможно загрузить содержимое.");
      //           break;
      //       }
      //     });
      //   });
      // }
    }
  });

  setSize(sliderElement, slideMargin);

  window.addEventListener('resize', function (e) {
    setSize(sliderElement, slideMargin);
  });

  Hash.on('^page\/([0-9]*)$', {
    yep: function(path, parts) {
      var page = parts[1];
      if (page!==undefined) {
        var maxPage = window.slider.getSlideCount() - 1;
        page = Math.abs(page);
        page = page > maxPage ? maxPage : page;
        window.slider.goToSlide(page);
      }
    },
    nop: function(path) {
      window.slider.goToSlide(0);
      Hash.go('page/0').update();
    }
  });

  document.getElementById("bgvid").play();
  lightbox.enable();
  lightbox.build();
  if (!$.isTouch) $(sliderElement + " " + slideElement + ":not(.cover)").mCustomScrollbar({ theme: "minimal-dark" });
});